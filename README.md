# Gentoo Containers

Gentoo Containers collection.


## About

A collection Gentoo-related of Container/Dockerfiles.


## Usage

### Build

Go to the desired image directory inside `src`
and execute:

``` shell
podman build --tag "$(basename "$(pwd)")" .
```

### Cleanup

``` shell
podman rmi "$(podman images --filter dangling=true --quiet)"
```

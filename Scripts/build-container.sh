#!/bin/sh


set -e

export PATH

trap 'exit 128' INT


image_name="${1}"

echo "image_name: ${image_name}"

if [ -n "${2}" ] ; then
    builder="${2}"
else
    builder="docker"
fi

echo "builder: ${builder}"

if [ -n "${3}" ] ; then
    ci_registy_image="${3}/"
else
    ci_registy_image=""
fi

echo "ci_registy_image: ${ci_registy_image}"


root="$(realpath "$(dirname "${0}")/../")"

echo "root: ${root}"

cd "${root}"


registy_image_name="${ci_registy_image}${image_name}:latest"

echo "registy_image_name: ${registy_image_name}"

${builder} pull "${registy_image_name}" ||
    echo "Image ${registy_image_name} not available."

cd "${root}"/Containers/"${image_name}"

builder_opts=" --file Containerfile --tag ${registy_image_name} "

if [ ${builder} = docker ] ; then
    builder_opts=" ${builder_opts} --cache-from ${registy_image_name} "
fi

echo "Builder (${builder}) options: ${builder_opts}"

# shellcheck disable=2086
${builder} build ${builder_opts} .
